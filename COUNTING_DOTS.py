import cv2 
#input image taking
path ='black.png'

# this will be reading the image in greyscale mode
gray = cv2.imread(path, 0) 

# threshold 
th, threshed = cv2.threshold(gray, 100, 255, 
		cv2.THRESH_BINARY|cv2.THRESH_OTSU) 

# findcontours 
cnts = cv2.findContours(threshed, cv2.RETR_LIST, 
					cv2.CHAIN_APPROX_SIMPLE)[-2] 

# filter by area s1 s2 
s1 = 3
s2 = 20
allcnts = [] 

for cnt in cnts: 
	if(s1<cv2.contourArea(cnt)<s2): 
		allcnts.append(cnt) 


# printing output 
print("\nDots number: {}".format(len(allcnts))) 

#show the popup image which has taken as input 
cv2.imshow('image',gray)
cv2.waitKey(0)
cv2.destroyAllWindows()
