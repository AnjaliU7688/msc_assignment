                         Counting Dots on Black From Black-background
-In COUNTING_DOTS.py file I AM trying to count the white dots from black background.
-I have read the image and convert it into greyscale mode as if any input image will be colorfull it will get converted to greyscale.
-I have used the function threshold() it will used to classify the pixel values.if the pixel value is greater than threshold value  it is assigned one value (may be white), else it is assigned another value (may be black).
-For finding the circle before we have to find the boundries of circle as you have discussed in lecture so I have used findContours() function .The contours are a useful tool for shape analysis and object detection and recognition for small scale images .
-contour is a Numpy array of (x, y) coordinates of boundary points of the object. it connect white dot for counting.
-I have used another function contourArea() calculate the contour area of the object
here object is white dot.here I have assign the area with s1 and s2 if the object satisfied the condition area to be count as dot then it will be push to array of allcnts[]
-finally got the count with the image which is taken as input.

Source Code:COUNTING_DOTS.py
Input:black.png

RUN:
python COUNTING_DOTS.py









